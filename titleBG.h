/*****************************************************************************
titleBG.h
Aythor	: 上野　匠
Data	: 2017_07_04
=============================================================================
Updata

*****************************************************************************/
#ifndef TITLEBG_H
#define TITLEBG_H

#include "scene.h"
#include "scene2D.h"

//////////////////////////////////////////////////////////////////////////////
//前方宣言
//////////////////////////////////////////////////////////////////////////////
class Scene2D;

//////////////////////////////////////////////////////////////////////////////
//TitleBGクラス宣言
//////////////////////////////////////////////////////////////////////////////
class TitleBG : public Scene2D
{
public:
	TitleBG(){}
	~TitleBG(){}

	HRESULT init(void);
	void uninit(void);
	void update(void);
	void draw(void);

	//セッター
	static TitleBG *create();

private:

};

#endif // !TITLEBG_H
