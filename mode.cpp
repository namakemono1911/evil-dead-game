/*****************************************************************************
mode.cpp
Aythor	: 上野　匠
Data	: 2017_06_28
=============================================================================
Updata

*****************************************************************************/
//////////////////////////////////////////////////////////////////////////////
//ヘッダーファイルインクルード
//////////////////////////////////////////////////////////////////////////////
#include "main.h"
#include "scene.h"
#include "light.h"
#include "imgui.h"

//タイトルに必要
#include "titleBG.h"

//ゲームに必要
#include "kamome.h"
#include "coin.h"
#include "mesh.h"
#include "meshField.h"
#include "sky.h"
#include "fade.h"
#include "zombie.h"
#include "reticle.h"
#include "stage.h"

//リザルトに必要
#include "resultBG.h"

#include "sound.h"
#include "mode.h"
#include "manager.h"

//////////////////////////////////////////////////////////////////////////////
//静的メンバ変数初期化
//////////////////////////////////////////////////////////////////////////////

/*
###################################################################################################################
## Mode class
###################################################################################################################
*/
Mode * Mode::create(Mode * mode)
{
	Mode *createMode;

	createMode = mode;

	createMode->init();

	return createMode;
}

/*
###################################################################################################################
## TitleMode class
###################################################################################################################
*/
//////////////////////////////////////////////////////////////////////////////
//関数名	: Title::init
//返り値	: 
//説明		: 初期化処理
//////////////////////////////////////////////////////////////////////////////
void Title::init(void)
{
	//現在のモード確定
	mType = TITLE_MODE;
	mChenge = false;

	//プレイヤー生成
	mPlayer = Player::create(new PlayerTitle);

	//タイトル背景生成
	TitleBG::create();

	//タイトルBGM再生
	Manager::getSound()->Play(CSound::TITLE_BGM001);
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Title::uninit
//返り値	: 
//説明		: 終了
//////////////////////////////////////////////////////////////////////////////
void Title::uninit(void)
{
	//オブジェクト終了
	Scene::uninitAll();

	//BGM終了
	Manager::getSound()->Stop();
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Title::update
//返り値	: 
//説明		: 更新
//////////////////////////////////////////////////////////////////////////////
void Title::update(void)
{
	//オブジェクト更新
	Scene::updateAll();

	if (mChenge == true)
		Manager::setMode(new Game);
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Title::draw
//返り値	: 
//説明		: 描画
//////////////////////////////////////////////////////////////////////////////
void Title::draw(void)
{
	//オブジェクト描画
	Scene::drawAll();
}

/*
###################################################################################################################
## GameMode class
###################################################################################################################
*/
//////////////////////////////////////////////////////////////////////////////
//静的メンバ変数初期化
//////////////////////////////////////////////////////////////////////////////
Player	*Game::mPlayer = NULL;
Light	*Game::mLight = NULL;
Stage	*Game::mStage = NULL;

//////////////////////////////////////////////////////////////////////////////
//関数名	: Game::init
//返り値	: 
//説明		: 初期化処理
//////////////////////////////////////////////////////////////////////////////
void Game::init(void)
{
	//変数初期化
	mType = GAME_MODE;
	mChenge = false;

	//カメラ生成
	Camera::create(
		D3DXVECTOR3(0.f, 50.f, -30.f),
		D3DXVECTOR3(0.f, 0.f, 0.f),
		D3DXVECTOR3(0.f, 1.f, 0.f),
		-30);

	//プレイヤー生成
	mPlayer = Player::create(new PlayerGame);


	//ライト生成
	/*Light::setLight(
	D3DXVECTOR3(0.f, 10.f, 0.f),
	D3DXVECTOR3(0.f, 0.f, 0.f),
	D3DCOLOR_RGBA(0, 0, 0, 0),
	D3DCOLOR_RGBA(255, 255, 255, 192));*/

	Light::setLight(
		D3DXVECTOR3(0.f, 10.f, 10.f),
		D3DXVECTOR3(0.f, -0.5f, -1.f),
		D3DCOLOR_RGBA(255, 255, 255, 255),
		D3DCOLOR_RGBA(255, 255, 255, 255));

	//フィールド
	MeshField::loadTex();

	//空
	Sky::loadTex();

	//ゾンビ生成
	Zombie::create(Dvec3(-73.f, 0.f, 96.f), Dvec3(0, 0, 0), Dvec3(1,1,1));
	/*Zombie::create(Dvec3(46.f, 0.f, -92.f), Dvec3(PI, 0, 0), Dvec3(1, 1, 1));
	Zombie::create(Dvec3(267.f, 0.f, 39.f), Dvec3(PI * 0.5f, 0, 0), Dvec3(1, 1, 1));*/

	mStage = Stage::create();
	mStage->loadMapFile("data/map/hole.map");

	//BGM再生
	Manager::getSound()->Play(CSound::GAME_BGM001);
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Game::uninit
//返り値	: 
//説明		: 終了
//////////////////////////////////////////////////////////////////////////////
void Game::uninit(void)
{
	//ライト終了
	Light::releaseAll();

	//オブジェクト終了
	Scene::uninitAll();

	//BGM終了
	Manager::getSound()->Stop();
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Game::update
//返り値	: 
//説明		: 更新
//////////////////////////////////////////////////////////////////////////////
void Game::update(void)
{
	if (mChenge == true)
		Manager::setMode(new Result);
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Game::draw
//返り値	: 
//説明		: 描画
//////////////////////////////////////////////////////////////////////////////
void Game::draw(void)
{
	//オブジェクト描画
	Scene::drawAll();
}

/*
###################################################################################################################
## GameMode class
###################################################################################################################
*/
//////////////////////////////////////////////////////////////////////////////
//関数名	: Result::init
//返り値	: 
//説明		: 初期化
//////////////////////////////////////////////////////////////////////////////
void Result::init(void)
{
	//現在のモード確定
	mType = RESULT_MODE;
	mChenge = false;

	//プレイヤー生成
	mPlayer = Player::create(new PlayerResult);

	//リザルト背景生成
	ResultBG::create();
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Result::uninit
//返り値	: 
//説明		: 終了
//////////////////////////////////////////////////////////////////////////////
void Result::uninit(void)
{
	//オブジェクト終了
	Scene::uninitAll();
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Result::update
//返り値	: 
//説明		: 更新
//////////////////////////////////////////////////////////////////////////////
void Result::update(void)
{
	//オブジェクト更新
	Scene::updateAll();

	if (mChenge == true)
		Manager::setMode(new Title);
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: Result::draw
//返り値	: 
//説明		: 描画
//////////////////////////////////////////////////////////////////////////////
void Result::draw(void)
{
	//オブジェクト描画
	Scene::drawAll();
}
