/*****************************************************************************
resultBG.cpp
Aythor	: 上野　匠
Data	: 2017_07_06
=============================================================================
Updata

*****************************************************************************/
//////////////////////////////////////////////////////////////////////////////
//ヘッダーファイルインクルード
//////////////////////////////////////////////////////////////////////////////
#include "main.h"
#include "resultBG.h"

//////////////////////////////////////////////////////////////////////////////
//関数名	: TitleBG::init
//返り値	: 
//説明		: 初期化処理
//////////////////////////////////////////////////////////////////////////////
HRESULT ResultBG::init(void)
{
	loadTex("data/texture/resultBG.jpg");
	Scene2D::init();
	return S_OK;
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: ResultBG::uninit
//返り値	: 
//説明		: 終了
//////////////////////////////////////////////////////////////////////////////
void ResultBG::uninit(void)
{
	Scene2D::uninit();
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: ResultBG::update
//返り値	: 
//説明		: 更新
//////////////////////////////////////////////////////////////////////////////
void ResultBG::update(void)
{
	Scene2D::update();
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: ResultBG::draw
//返り値	: 
//説明		: 描画
//////////////////////////////////////////////////////////////////////////////
void ResultBG::draw(void)
{
	Scene2D::draw();
}

//////////////////////////////////////////////////////////////////////////////
//関数名	: ResultBG::create
//返り値	: 
//説明		: 生成
//////////////////////////////////////////////////////////////////////////////
ResultBG * ResultBG::create(void)
{
	ResultBG *result;

	result = new ResultBG;
	result->setAll(
		D3DXVECTOR3(SCREEN_WIDTH * 0.5f, SCREEN_HEIGHT * 0.5f, 0.f),
		D3DXVECTOR3(SCREEN_WIDTH, SCREEN_HEIGHT, 0.f),
		D3DXVECTOR2(0.f, 0.f),
		D3DXVECTOR2(1.f, 1.f));

	result->init();

	return result;
}
